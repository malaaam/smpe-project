LungCapData<-read.delim("~/Desktop/LungCapData.txt")
attach(LungCapData)
summary(LungCapData)
head(LungCapData)
str(LungCapData)
library(ggplot2)
qplot(Age,Height,data=LungCapData)

summary(LungCapData$Smoke)

MaleData<-LungCapData[Gender=="male",]
FemData<-LungCapData[Gender=="female",]
AllSmoke<- LungCapData[Smoke=="yes",]
MaleSmoke<-LungCapData[Gender=="male"& Smoke=="yes",]
FemSmoke<-LungCapData[Gender=="female"& Smoke=="yes",]
Smoke18<- LungCapData[Age>18,]
MaleOver15<-LungCapData[Gender=="male"& Age>15,]
FemOver15<-LungCapData[Gender=="female"& Age>15,]
AgeGroups<-cut(Age,breaks=c(0,13,15,17,25),labels=c("<13","14/15","16/17","18+"))
MoreData<-cbind(LungCapData,AgeGroups)
NumData<-data.frame(LungCap,Age,Height)
cor(~Gender)
pairs(NumData)

#Examine the realationship bitween gender and smoking
#Produce contingency table to make the bar

Table1<-table(Smoke,Gender)

barplot(Table1,beside=1,legend.text=c("Nonsmoker","Smoker"),main="Gender and Smoking",
        xlab="Gender", ylab="Smoking",las=1,col=c(3,2))

plot=ggplot(LungCapData,aes(x=Gender))+geom_bar(fill="#880011")+
  ggtitle("")+labs(x="", y="Count ")
plot

plot=ggplot(LungCapData,aes(x=Smoke))+geom_bar(fill="#880011")+
      ggtitle("Non-smoke Vs Smoke")+labs(x="Smoke", y="People Countn ")+
      facet_wrap(~Gender)
plot

mosaicplot(Table1,main="Gender and Smoking",col=c(3,2))

#Pearsons correlation is used to examine the strength of linear relationship bitween 2 
#numeric veriables

#Scatter plot Age Vs.Height (All)
plot=ggplot(data=LungCapData, aes(x=LungCapData$Age,y=LungCapData$Height)) +
  geom_point(alpha=.4, size=4, color="#880011")+ 
  labs(y = "Hight", x = "Age") +geom_smooth(method= "lm",color= "Black")+
  ggtitle("Age vs. Hight")
plot

#Scatter plot Age Vs.Height (Gender)
plot=ggplot(data=LungCapData, aes(x=LungCapData$Age,y=LungCapData$Height)) +
  geom_point(alpha=.4, size=4, color="#880011")+
  ggtitle("LungCap Vs.Age")+
  labs(y = "Hight", x = "Age")+
  geom_smooth(method= "lm",color= "Black")+
  facet_wrap(~ LungCapData$Gender)+theme(legend.title=element_blank())
plot
cor(LungCapData$Age,LungCapData$Height)

#Scatter plot for LungCap Vs.Age (All Smoker)
plot=ggplot(data = AllSmoke, aes(x=AllSmoke$Age, y=AllSmoke$LungCap))+
  geom_point(alpha=.4, size=4, color="#880011") + labs(y = "LungCap", x = "age")+
  geom_smooth(method= "lm",color= "black")+
  ggtitle("Smoke vs. LungCap,All")+
  facet_wrap(~ AllSmoke$Gender)+theme(legend.title=element_blank())
plot
cor(AllSmoke$Age,AllSmoke$LungCap)

#Scatter plot for LungCap Vs.Age (Male Smoker)
plot=ggplot(data = MaleSmoke, aes(x=MaleSmoke$Age, y=MaleSmoke$LungCap))+
  geom_point(alpha=.4, size=4, color="#880011") + labs(y = "LungCap", x = "age")+
  geom_smooth(method= "lm",color= "black")+
  ggtitle("Smoke vs. LungCap,Male")
plot
cor(MaleSmoke$Age,MaleSmoke$LungCap)

#Scatter plot for LungCap Vs.Age (Female Smoker)
plot=ggplot(data = FemSmoke, aes(x=FemSmoke$Age, y=FemSmoke$LungCap))+
  geom_point(alpha=.4, size=4, color="#880011") + labs(y = "LungCap", x = "age")+
  geom_smooth(method= "lm",color= "black")+
  ggtitle("Smoke vs. LungCap, Female")
plot
cor(FemSmoke$Age,FemSmoke$LungCap)

#LungCap Non-Smoke Vs.Smoke
plot=ggplot(data = LungCapData, aes(x=LungCapData$Age, y=LungCapData$LungCap))+
  geom_point(alpha=.4, size=1, color="#880011") + labs(y = "LungCap", x = "age")+
  geom_smooth(method= "gam",color= "black")+
  ggtitle("Non-Smoke vs. Smoke LungCap ")+
  facet_wrap(~Smoke)
plot

#LungCap of Smoker 
plot=ggplot(data = AllSmoke, aes(x="", y=AllSmoke$LungCap))+
  geom_boxplot(fill = "white",varwidth = TRUE, colour = "#3366FF") + labs(y = "LungCap", x = "age")+
  geom_smooth(method= "gam",color= "black")+
  facet_wrap(~Caesarean)
plot

# LungCap of Smoker Agegroup

plot=ggplot(data=MoreData,aes(x=MoreData$Age,y=MoreData$LungCap,colour=AgeGroups))+
  geom_point()+geom_smooth(method="lm",color="black")+
  labs(y="Lung Capacity",x="age")+facet_grid(Smoke~AgeGroups)
plot

plot=ggplot(data = MoreData, aes(x=MoreData$Age, y=MoreData$LungCap))+
  geom_boxplot()+labs(y = "Lung Capacity", x = "Smoke")+
  facet_grid(AgeGroups~Smoke)
plot

#LungCap of Smoker 
meanL <- mean(LungCapData$LungCap)
std <- sd(LungCapData$LungCap)
LungCapPlot = ggplot(data = LungCapData, aes(LungCapData$LungCap)) +
  geom_bar(fill="white", colour = "black") + labs(x= "Lung Capacity", y = "count") +
  geom_vline(aes(xintercept = meanL, colour = "mean")) +
  geom_vline(aes(xintercept = (meanL + std), colour = "std"), linetype = "dashed") +
  geom_vline(aes(xintercept = (meanL - std), colour = "std"), linetype = "dashed") +
  geom_vline(aes(xintercept = median(LungCapData$LungCap),colour = "median"), linetype = "dashed") +
  scale_colour_manual(name = "Legend",
                      breaks = c("mean", "std","median"),
                      values= c(mean = "red", std = "blue", median = "orange"))
LungCapPlot


# Age Variable Analysis
meanA <- mean(LungCapData$Age)
std <- sd(AllSmoke$Age)
AgePlot = ggplot(data = LungCapData, aes(LungCapData$Age)) +
  geom_bar(fill="white", colour = "black") + labs(x= "Age (years)", y = "people") +
  geom_vline(aes(xintercept = meanA, colour = "mean")) +
  geom_vline(aes(xintercept = (meanA + std), colour = "std"), linetype = "dashed") +
  geom_vline(aes(xintercept = (meanA - std), colour = "std"), linetype = "dashed") +
  geom_vline(aes(xintercept = median(LungCapData$Age),colour = "median"), linetype = "dashed") +
  scale_colour_manual(name = "Legend",
                      breaks = c("mean", "std","median"),
                      values= c(mean = "red", std = "blue", median = "orange"))
AgePlot

#Height Variable Analysis
meanH <- mean(LungCapData$Height)
std <- sd(LungCapData$Height)
HeightPlot = ggplot(data = LungCapData, aes(LungCapData$Height)) +
  geom_bar(fill="white", colour = "black") + labs(x= "Height (inches)", y = "people")+
  geom_vline(aes(xintercept = meanH, colour = "mean"))+
  geom_vline(aes(xintercept = (meanH + std), colour = "std"), linetype = "dashed")+
  geom_vline(aes(xintercept = (meanH - std), colour = "std"), linetype = "dashed")+
  geom_vline(aes(xintercept = median(LungCapData$Height),
                 colour = "median"), linetype = "dashed")+
  scale_colour_manual(name = "Legend",
                      breaks = c("mean", "std","median"),
                      values= c(mean = "red", std = "blue", median = "orange"))
HeightPlot



#LungCap vs.Age

plot=qplot(factor(Age),LungCap,data = LungCapData, geom = "boxplot")+geom_jitter(width = 0.2)+
ggtitle("Boxplot of LungCap vs. Age")+labs(y = "LungCap", x = "Age")
plot

plot <- ggplot(LungCapData, aes(factor(Age),LungCap))+
  geom_boxplot(fill ="white",varwidth=TRUE,colour="#3366FF")+
  labs(y="Lung Capacity",x="Age")
plot

plot <- ggplot(LungCapData, aes(Age,LungCap,colour=Gender))+
  geom_point()+geom_smooth(method="lm",color="black")+
  labs(y="Lung Capacity",x="Age")
plot

cor(LungCap,Height)

#LungCap vs.Height

plot <- ggplot(LungCapData, aes(Height,LungCap,colour=Gender))+
  geom_point()+stat_smooth(colour='blue',span=0.2)+  
  labs(y="Lung Capacity",x="Height")
plot


# boxplot(LungCap, main="Boxplot", ylab="Lung Capacity", ylim=c(0,18),las=1)
#boxplot(LungCap~Gender, main="Lung Capacity by Gender", ylab="Lung Capacity", ylim=c(0,16),las=1)
#boxplot(LungCap~Smoke, main="LungCap Vs. Smoke", ylab="Lung Capacity",las=1)

#LungCap vs.Gender

plot <- ggplot(LungCapData,aes("",LungCap))+
  geom_boxplot(varwidth=TRUE,aes(colour=Gender))+labs(y="Lung Capacity")+
  facet_wrap(~Gender)+geom_jitter(width=0.2)+theme(axis.title.x=element_blank())
plot

#LungCap vs.Smoke

plot <- ggplot(LungCapData,aes("",LungCap))+
  geom_boxplot(varwidth=TRUE,aes(colour=Smoke))+
  labs(y="Lung Capacity")+facet_wrap(~Smoke)+geom_jitter(width=0.2)+
  theme(axis.title.x = element_blank())
plot


#LungCap vs.Smoke (18+)

plot <- ggplot(Smoke18, aes("",Smoke18$LungCap))+geom_boxplot(aes(colour=Smoke))+
  labs(y="Lung Capacity of 18+")+facet_wrap(~Smoke18$Smoke)+
  theme(axis.title.x=element_blank())
plot


#LungCap vs.Caesarean

plot <- ggplot(LungCapData, aes("",LungCap))+
  geom_boxplot(varwidth=TRUE,aes(colour=Caesarean))+
  labs(y="Lung Capacity")+facet_wrap(~Caesarean)+geom_jitter(width=0.2)+
  theme(axis.title.x = element_blank())
plot

#LungCap vs.Caesarean for smoker

plot <- ggplot(data=AllSmoke, aes("",AllSmoke$LungCap))+
  geom_boxplot(varwidth=TRUE,aes(colour=Caesarean))+
  labs(y="Lung Capacity")+facet_grid(AllSmoke$Smoke~AllSmoke$Caesarean)+
  geom_jitter(width=0.2)+theme(axis.title.x=element_blank())
plot

#Box Ploting LungCap Vs.Age by gender
plot=ggplot(data=LungCapData,aes(x=factor(Age),y=LungCap))+
  geom_boxplot(outlier.colour = "red", outlier.size = 2)+
     ggtitle("Boxplot of LungCap Vs. Age")+ labs(y = "LungCap", x = "Age")+
     facet_wrap(~Gender)
plot

#Boxplot plot for LungCap of Smoker 
plot=ggplot(data = AllSmoke, aes(x=factor(AllSmoke$Age), y=AllSmoke$LungCap))+
  geom_boxplot() + labs(y = "LungCap", x = "age")+
  geom_smooth(method= "gam",color= "black")+
  ggtitle("Caesarean Smoker LungCap ")+
  facet_wrap(~Caesarean)
plot

# Hypothesis 1: Exemine the relationship between Smoking and Lung Capacity within the age group


# On Avarege, smoker are older than non smoker
# Older children has big body so they have big lunch capacity.

plot(LungCap[Age>=18]~Smoke[Age>=18], 
        main="LungCap Vs. Smoke, for 18+", ylab="Lung Capacity",las=1)

boxplot(LungCap~Smoke*AgeGroups, 
        main="",ylab="Lung Capacity",las=2,col=c(4,2))

hist(LungCap, freq=FALSE, prob= TRUE, ylim = c(0,0.2),breaks= 14,
     main="", xlab="Lung Capacity",
     las=1,lines(density(LungCap),col=3,lwd=3))

# It is often of interest to quantify the center and spread of the distribution of a veriable.
# Here we will summarize categorical veriable Smoke and numerical veriable Lung Capacity
table(Smoke)/length(Smoke)

mean(LungCap)
median(LungCap)
var(LungCap)
sd(LungCap)
sqrt(var(LungCap))
min(LungCap)
max(LungCap)
range(LungCap)

#Linear Regression

reg= lm(data = LungCapData, formula = LungCap~Age)
summary(reg)

plot = ggplot(data = LungCapData, aes(x=LungCapData$Age,y=LungCapData$LungCap,colour=Age))+
  geom_point()+labs(y="Ling Capacity",x="Age")+ 
  geom_smooth(method="lm",color="black")
plot

cor(LungCap,Age)
confint(reg,conf.level=0.95)

#Multiple Linear Regression


reg1= lm(data=LungCapData, 
        formula=LungCap~Age+Height)
summary(reg1)

cor(Age,Height,method = "pearson")
confint(reg1,conf.level=0.95)

#Multiple Linear Regression with all veriables

reg2= lm(data = LungCapData, 
         formula = LungCap~Age+Height+Smoke+Gender+Caesarean)
summary(reg2)

cor(LungCap,Age,Height)
confint(reg2,conf.level=0.95)


model<-lm(LungCap~Age)
summary(model)
plot(model)
cor(Age,LungCap)
attributes(mod)

#Examine the assumtion for a model of the relationship between Age and Lung Capacity
#Lung Capacity =Y, Outcome or Dependent Veriable

mod1<-lm(LungCap~Age+Height)
summary(mod1)
plot(mod1)
cor(Age,LungCap)
confint(mod1,conf.lavel=0.95)

#Partial F Test
#Example1
mod2<-lm(LungCap~Age+Height+Smoke)
mod3<-lm(LungCap~Age+Height)
anova(mod3,mod2)


#Example 2 
Full.Model<-lm(LungCap~Age+Height+Smoke+Gender+Caesarean)
summary(Full.Model)

Reduced.Model<-lm(LungCap~Age+Height)
summary(Reduced.Model)

anova(Reduced.Model,Full.Model)
install.packages("Rcpp")


help(boxplot)