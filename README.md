Project for the Scientific Methodology and Performance Evaluation course (2016-2017).

Student: Mahabub Alam

SUBJECT: A study about Lung Capacity

In this report I use the lung capacity dataset collected from internet. This dataset captured the data about human lung capacity which could be influenced by the internal and the external factor like individuals age, height, smoking habits and so on. This dataset contains 725 observation and 6 different variables.

SUMMARY:

In this work, I would like to identify how different factors like age, height, smoking, gender, caesarean can impact on the lung capacity of the people. More specifically, I would like to identify-

H1: Does human age have any effect on the lung capacity?
H2: Are heights related to the effect on the lung capacity?
H3: Does smoker have the same lung capacity as well as who do not smoke?  
H4: Does caesarean people who smoke effected much compared to the non-cesarean people?

Finally, I would like to develop a good model which fits best with this dataset and can predict the human lung capacity considering the other relevant factors.  


HOW TO REPLICATE:

In the folder script, there is a R script that treat and save the data in the exact form.

COMMENTS:

I provided a PDF version of my report.